### create network
resource "hcloud_network" "k8s-net0" {
  name     = "${var.network-name}"
  ip_range = "${var.k8s_network}0/24"
}

### add subnet to network
resource "hcloud_network_subnet" "k8s-subnet0" {
  network_id   = hcloud_network.k8s-net0.id
  type         = "cloud"
  network_zone = "eu-central"
  ip_range = "${var.k8s_network}0/24"
}

### add network to controlplane nodes
resource "hcloud_server_network" "controlplane" {
  count      = var.controlplane-instances
  server_id  = hcloud_server.controlplane[count.index].id
  network_id = hcloud_network.k8s-net0.id
  ip         = "10.17.3.20${count.index}"
}

### add network to worker nodes
resource "hcloud_server_network" "worker" {
  count      = var.worker-instances
  server_id  = hcloud_server.worker[count.index].id
  network_id = hcloud_network.k8s-net0.id
  ip         = "10.17.3.22${count.index}"
}

