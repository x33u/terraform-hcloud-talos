## == controlplane
resource "hcloud_firewall" "controlplane_fw" {
    name   = "controlplane"
    rule {
      direction   = "in"
      description = "allow icmp"
      protocol    = "icmp"
      source_ips  = [
        "0.0.0.0/0",
        "::/0"
      ]
    }
    rule {
      direction = "in"
      description = "allow talosctl"
      protocol  = "tcp"
      port      = "50000-50001"
      source_ips = [
        "10.17.3.0/24",
        "212.172.3.214/32"
      ]
    }
    rule {
      direction = "in"
      description = "allow kubectl"
      protocol  = "tcp"
      port      = "6443"
      source_ips = [
        "10.17.3.0/24"
      ]
    }
}
resource "hcloud_firewall_attachment" "controlplane_ref" {
    firewall_id     = hcloud_firewall.controlplane_fw.id
    label_selectors = ["type=controlplane"]
}

## == worker
resource "hcloud_firewall" "worker_fw" {
    name   = "worker"
    rule {
      direction = "in"
      protocol  = "icmp"
      source_ips = [
        "0.0.0.0/0",
        "::/0"
      ]
    }
}
resource "hcloud_firewall_attachment" "worker_ref" {
    firewall_id     = hcloud_firewall.worker_fw.id
    label_selectors = ["type=worker"]
}
