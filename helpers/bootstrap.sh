#!/bin/bash

export $(cat .env | xargs)
export controlplane_0=$(hcloud server list | grep $controlplane_name | awk '//{print $4 }')
#export controlplane_0=$(hcloud load-balancer list | grep $controlplane_name | awk '//{print $4 }')

./images/"$talos_version"/talosctl-linux-amd64 \
  --talosconfig talos/talosconfig \
  config endpoint $controlplane_0

./images/"$talos_version"/talosctl-linux-amd64 \
  --talosconfig talos/talosconfig \
  config node $controlplane_0

until ./images/"$talos_version"/talosctl-linux-amd64 --talosconfig talos/talosconfig bootstrap &> /dev/null
  do
    echo "waiting for nodes become ready to bootstrapping ..."
    sleep 2
done

## == create kubeconfig
if [ -f talos/kubeconfig ]; then
    echo 'kubeconfig already in place'
else
  ./images/"$talos_version"/talosctl-linux-amd64 --talosconfig talos/talosconfig kubeconfig talos/kubeconfig
fi
