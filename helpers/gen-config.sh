#!/bin/bash

export $(cat .env | xargs)

## create image dir
if [ ! -d images/$talos_version ]; then
  mkdir -p images/$talos_version
fi


## == get talosctl and make it executable
if [ -f images/$talos_version/talosctl-linux-amd64 ]; then
    echo '... "talosctl-linux-amd64" already exists'
else
    echo -e '\e[0;32m downloading "talosctl-linux-amd64"\e[0m' ;
    curl https://github.com/siderolabs/talos/releases/download/$talos_version/talosctl-linux-amd64 \
      -L -o images/$talos_version/talosctl-linux-amd64 ; chmod +x images/$talos_version/talosctl-linux-amd64
fi

## create talos config dir
if [ ! -d talos ]; then
  mkdir talos
fi

## == generate talos secrets file
if [ -f talos/secrets.yaml ]; then
    echo '... "secrets.yaml" already exist'
else
    ./images/"$talos_version"/talosctl-linux-amd64 gen secrets \
    --output-file talos/secrets.yaml
fi


## == generate talos config files
if [ -f talos/talosconfig ]; then
    echo '... "talosconfig" already exist'
else
    ./images/"$talos_version"/talosctl-linux-amd64 gen config \
    "$cluster_name" \
    https://"$controlplane_dns":6443 \
    --with-secrets talos/secrets.yaml \
    --with-kubespan \
    --output-dir talos/ \
    --config-patch-control-plane @helpers/talos-controlplane-patch.yaml \
    --config-patch-worker @helpers/talos-worker-patch.yaml \
    && sleep 2 \
    && ./images/"$talos_version"/talosctl-linux-amd64 validate \
       --mode metal \
       --config talos/controlplane.yaml \
    && ./images/"$talos_version"/talosctl-linux-amd64 validate \
       --mode metal \
       --config talos/worker.yaml
fi



