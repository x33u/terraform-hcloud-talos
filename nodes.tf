### controlplane config
resource "hcloud_server" "controlplane" {
  count       = var.controlplane-instances
  name        = "controlplane-${count.index}"
  image       = var.controlplane-os_type
  server_type = var.controlplane-server_type
  location    = var.controlplane-location
  labels = {
    type = "controlplane"
  }
  user_data = file("talos/controlplane.yaml")
##  user_data = var.CONTROLPLANE_CONFIG
}

### worker config
resource "hcloud_server" "worker" {
  count       = var.worker-instances
  name        = "worker-${count.index}"
  image       = var.worker-os_type
  server_type = var.worker-server_type
  location    = var.controlplane-location
  labels = {
    type = "worker"
  }
  user_data = file("talos/worker.yaml")
##  user_data = var.WORKER_CONFIG
}
