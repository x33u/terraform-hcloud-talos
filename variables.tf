
#variable "hcloud_token" {
#  sensitive = true
#  default = ""
#}

# Configure the Hetzner Cloud Provider
#provider "hcloud" {
#  token = var.hcloud_token
#}


### os type
## == talos cluster name
variable "cluster-name" {
  default = "local-talos-cluster"
}
variable "controlplane-dns" {
  default = "cl-test-01.x33u.org"
}
variable "talos-version" {
  type    = string
  default = "v1.3.2"
}
variable "controlplane-os_type" {
  default = "96130368"
}
variable "worker-os_type" {
  default = "96130368"
}

## == private network variables
variable "network-name"{
  default = "k8snet-0"
}
variable "domain-name"{
  default = "k8s-0.local"
}
variable "k8s_network"{
  default = "10.17.3." #first three octets are customizable
}




### instances
variable "controlplane-instances" {
  default = "3"
}
variable "worker-instances" {
  default = "3"
}
variable "data-controlplane-instances" {
  default = "3" # same as controlplane-instances
}
variable "data-worker-instances" {
  default = "3" # same as worker-instances
}


### location
variable "controlplane-location" {
  default = "fsn1"
}
variable "worker-location" {
  default = "fsn1"
}
variable "data-location" {
  default = "fsn1"
}

### data size
variable "data-controlplane-disk_size" {
  default = "10"
}
variable "data-worker-disk_size" {
  default = "10"
}

### server type
variable "controlplane-server_type" {
  default = "cpx21"
}
variable "worker-server_type" {
  default = "cpx21"
}
