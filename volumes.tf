# controlplane  config
resource "hcloud_volume" "data_ma_volume" {
  count    = var.data-controlplane-instances
  name     = "data-controlplane-volume-${count.index}"
  size     = var.data-controlplane-disk_size
  location = var.data-location
#  format   = "ext4"
  automount = false
  delete_protection = false
}
resource "hcloud_volume_attachment" "data_ms_vol_attachment" {
  count     = var.data-controlplane-instances
  volume_id = hcloud_volume.data_ma_volume[count.index].id
  server_id = hcloud_server.controlplane[count.index].id
  automount = false
}


# worker config
resource "hcloud_volume" "data_wo_volume" {
  count    = var.data-worker-instances
  name     = "data-worker-volume-${count.index}"
  size     = var.data-worker-disk_size
  location = var.data-location
#  format   = "ext4"
  automount = false
  delete_protection = false
}

resource "hcloud_volume_attachment" "data_wo_vol_attachment" {
  count     = var.data-worker-instances
  volume_id = hcloud_volume.data_wo_volume[count.index].id
  server_id = hcloud_server.worker[count.index].id
  automount = false
}
