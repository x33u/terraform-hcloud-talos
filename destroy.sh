#!/bin/bash

terraform destroy --auto-approve

sh helpers/del-config.sh

sh helpers/del-image.sh
