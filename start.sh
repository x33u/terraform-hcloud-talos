#!/bin/bash

sh helpers/gen-config.sh

terraform apply --auto-approve

sh helpers/bootstrap.sh
