### load balancer ctl
resource "hcloud_load_balancer" "load_balancer0" {
  name               = "controlplane-nodes"
  load_balancer_type = "lb11"
  location           = "fsn1"
#  delete_protection = true
  labels = {
    type = "controlplane"
  }
}

resource "hcloud_load_balancer_network" "srvnetwork" {
  load_balancer_id = hcloud_load_balancer.load_balancer0.id
  network_id       = hcloud_network.k8s-net0.id
  ip               = "10.17.3.100"
  enable_public_interface = true
}

resource "hcloud_load_balancer_service" "k8s-service" {
    load_balancer_id = hcloud_load_balancer.load_balancer0.id
    protocol         = "tcp"
    listen_port      = "6443"
    destination_port = "6443"
}
resource "hcloud_load_balancer_service" "talos-service" {
    load_balancer_id = hcloud_load_balancer.load_balancer0.id
    protocol         = "tcp"
    listen_port      = "50000"
    destination_port = "50000"
}


resource "hcloud_load_balancer_target" "load_balancer_target" {
  depends_on  = [hcloud_server.controlplane]
  type             = "label_selector"
  load_balancer_id = hcloud_load_balancer.load_balancer0.id
  use_private_ip = true
  label_selector = "type=controlplane"
}
